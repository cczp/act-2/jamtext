#include <string.h>

#include "jamtext-internal.h"

struct jam_deserializer *jam_deserializer_new(FILE *file) {
	struct jam_deserializer *deserializer = malloc(sizeof(struct jam_deserializer));
	
	if (deserializer == NULL) {
		return NULL;
	}
	
	deserializer->type = JAM_PROCESSOR_FILE;
	
	deserializer->file = file;
	
	deserializer->line = 1;
	deserializer->column = 0;
	
	deserializer->prev_char = 0;
	deserializer->current_char = 0;
	
	deserializer->error = "";
	
	return deserializer;
}
struct jam_deserializer *jam_deserializer_new_from_buffer(
	const char *buffer,
	size_t size
) {
	struct jam_deserializer *deserializer = malloc(sizeof(struct jam_deserializer));
	
	if (deserializer == NULL) {
		return NULL;
	}
	
	deserializer->type = JAM_PROCESSOR_BUFFER;
	
	deserializer->buffer = buffer;
	
	deserializer->cursor = 0;
	deserializer->buffer_size = size;
	
	deserializer->line = 1;
	deserializer->column = 0;
	
	deserializer->prev_char = 0;
	deserializer->current_char = 0;
	
	deserializer->error = "";
	
	return deserializer;
}
struct jam_deserializer *jam_deserializer_new_from_string(
	const char *str
) {
	return jam_deserializer_new_from_buffer(str, strlen(str));
}

struct jam_value *jam_deserializer_run(struct jam_deserializer *deserializer) {
	return jam_get_value(deserializer);
}

const char *jam_deserializer_get_error(struct jam_deserializer *deserializer) {
	return deserializer->error;
}
size_t jam_deserializer_get_line(struct jam_deserializer *deserializer) {
	return deserializer->line;
}
size_t jam_deserializer_get_column(struct jam_deserializer *deserializer) {
	return deserializer->column;
}
size_t jam_deserializer_get_cursor(struct jam_deserializer *deserializer) {
	return deserializer->cursor;
}

int jam_next_char(struct jam_deserializer *deserializer) {
	// We need to remember the previous character so we can correctly
	// parse flavors and numbers, which can be interrupted by a special
	// character
	
	if (deserializer->prev_char) {
		deserializer->current_char = deserializer->prev_char;
		deserializer->prev_char = 0;
	} else {
		if (deserializer->type == JAM_PROCESSOR_FILE) {
			deserializer->current_char = fgetc(deserializer->file);
		} else {
			if (deserializer->cursor >= deserializer->buffer_size) {
				deserializer->current_char = EOF;
			} else {
				deserializer->current_char = deserializer->buffer[
					deserializer->cursor
				];
				
				deserializer->cursor++;
			}
		}
		
		if (deserializer->current_char == '\n') {
			deserializer->line++;
			deserializer->column = 0;
		} else {
			deserializer->column++;
		}
	}
	
	return deserializer->current_char;
}
