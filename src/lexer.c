#include <ctype.h>
#include <errno.h>
#include <string.h>

#include "jamtext-internal.h"

#define die(message)\
	deserializer->error = message;\
	token.type = JAM_TOKEN_INVALID;\
	return token;

const char *JAM_WORD_NULL = "null";
const char *JAM_WORD_TRUE = "true";
const char *JAM_WORD_FALSE = "false";

const char *JAM_WORD_CHILDREN = "_children";

const char *JAM_HEX_DIGITS = "0123456789abcdef";
const char *JAM_SPECIAL_CHARS = " \t\n\r#\"[]{},:";

static char *unexpect(char chr) {
	switch (chr) {
	case JAM_CHAR_NULL:
		return "unexpected null character";
	case JAM_CHAR_EOF:
		return "unexpected end of file";
	default:
		return "unexpected character";
	}
}

static struct jam_token tokenize_string(struct jam_deserializer *deserializer) {
	struct jam_token token;
	
	token.type = JAM_TOKEN_STRING;
	
	char *buffer = malloc(sizeof(char));
	char to_add[5];
	
	if (buffer == NULL) {
		die("out of memory");
	}
	
	buffer[0] = '\0';
	to_add[0] = '\0';
	
	size_t size = 0, used = 0;
	
	while ((jam_next_char(deserializer)) != JAM_CHAR_QUOTE) {
		switch (deserializer->current_char) {
		case JAM_CHAR_NULL: // FALLTHROUGH
		case JAM_CHAR_EOF:
			die(unexpect(deserializer->current_char));
		case JAM_CHAR_BACKSLASH:
			switch (jam_next_char(deserializer)) {
			case JAM_CHAR_QUOTE:
				to_add[0] = '"';
				to_add[1] = '\0';
				
				break;
			case JAM_CHAR_BACKSLASH:
				to_add[0] = '\\';
				to_add[1] = '\0';
				
				break;
			case JAM_CHAR_ESCAPE_FROM_BACKSPACE:
				to_add[0] = '\b';
				to_add[1] = '\0';
				
				break;
			case JAM_CHAR_ESCAPE_FROM_FORM_FEED:
				to_add[0] = '\f';
				to_add[1] = '\0';
				
				break;
			case JAM_CHAR_ESCAPE_FROM_LF:
				to_add[0] = '\n';
				to_add[1] = '\0';
				
				break;
			case JAM_CHAR_ESCAPE_FROM_CR:
				to_add[0] = '\r';
				to_add[1] = '\0';
				
				break;
			case JAM_CHAR_ESCAPE_FROM_TAB:
				to_add[0] = '\t';
				to_add[1] = '\0';
				
				break;
			case JAM_CHAR_ESCAPE_FROM_UNICODE: {
				uint32_t ch = 0;
				
				for (size_t i = 0; i < 4; i++) {
					jam_next_char(deserializer);
					deserializer->current_char = tolower(deserializer->current_char);
					
					char *digit = strchr(JAM_HEX_DIGITS, deserializer->current_char);
					
					if (digit == NULL) {
						die("invalid hex digit");
					}
					
					ch +=
						(digit - JAM_HEX_DIGITS) <<
						((4 - (i + 1)) * 4);
				}
				
				jam_utf8_encode(to_add, ch);
				
				break;
			}
			default:
				to_add[0] = deserializer->current_char;
				to_add[1] = '\0';
				
				break;
			}
			
			break;
		default:
			to_add[0] = deserializer->current_char;
			to_add[1] = '\0';
			
			break;
		}
		
		size_t how_much = strlen(to_add);
		
		jam_grow_string(&buffer, &size, &used, how_much);
		
		if (buffer == NULL) {
			die("out of memory");
		}
		
		memcpy(buffer + used, to_add, how_much + 1);
		
		used += how_much;
	}
	
	token.contents.as_string = buffer;
	
	return token;
}
static struct jam_token tokenize_word(struct jam_deserializer *deserializer) {
	struct jam_token token;
	
	char *buffer = malloc(sizeof(char));
	char to_add[2];
	
	if (buffer == NULL) {
		die("out of memory");
	}
	
	buffer[0] = '\0';
	to_add[0] = '\0';
	
	size_t size = 0, used = 0;
	
	while (
		deserializer->current_char != JAM_CHAR_NULL &&
		deserializer->current_char != JAM_CHAR_EOF &&
		strchr(JAM_SPECIAL_CHARS, deserializer->current_char) == NULL
	) {
		to_add[0] = deserializer->current_char;
		to_add[1] = '\0';
		
		size_t how_much = strlen(to_add);
		
		jam_grow_string(&buffer, &size, &used, how_much);
		
		if (buffer == NULL) {
			die("out of memory");
		}
		
		memcpy(buffer + used, to_add, how_much + 1);
		
		used += how_much;
		
		jam_next_char(deserializer);
		
		if (deserializer->current_char == JAM_CHAR_NULL) {
			die(unexpect(deserializer->current_char));
		}
	}
	
	deserializer->prev_char = deserializer->current_char;
	
	if (
		isdigit(buffer[0]) ||
		(buffer[0] == '-' && isdigit(buffer[1]))
	) {
		char *dummy = NULL;
		
		double number = jam_strtod(buffer, &dummy);
		
		if (dummy[0]!= '\0' || errno == EINVAL || errno == ERANGE) {
			errno = 0;
			
			die("invalid number");
		}
		
		token.type = JAM_TOKEN_NUMBER;
		token.contents.as_number = number;
		
		free(buffer);
	} else {
		if (strcmp(buffer, JAM_WORD_NULL) == 0) {
			token.type = JAM_TOKEN_NULL;
			
			free(buffer);
		} else if (strcmp(buffer, JAM_WORD_TRUE) == 0) {
			token.type = JAM_TOKEN_BOOL;
			token.contents.as_bool = true;
			
			free(buffer);
		} else if (strcmp(buffer, JAM_WORD_FALSE) == 0) {
			token.type = JAM_TOKEN_BOOL;
			token.contents.as_bool = false;
			
			free(buffer);
		} else {
			token.type = JAM_TOKEN_FLAVOR;
			token.contents.as_string = buffer;
		}
	}
	
	return token;
}

struct jam_token jam_next_token(struct jam_deserializer *deserializer) {
	struct jam_token token;
	
	while (true) {
		jam_next_char(deserializer);
		
		switch (deserializer->current_char) {
		case JAM_CHAR_NULL:
			die("unexpected null character");
		case JAM_CHAR_EOF:
			token.type = JAM_TOKEN_EOF;
			
			return token;
		case JAM_CHAR_SPACE: // FALLTHROUGH
		case JAM_CHAR_TAB: // FALLTHROUGH
		case JAM_CHAR_LF: // FALLTHROUGH
		case JAM_CHAR_CR:
			continue;
		case JAM_CHAR_LIST_START:
			token.type = JAM_TOKEN_LIST_START;
			
			return token;
		case JAM_CHAR_LIST_END:
			token.type = JAM_TOKEN_LIST_END;
			
			return token;
		case JAM_CHAR_OBJECT_START:
			token.type = JAM_TOKEN_OBJECT_START;
			
			return token;
		case JAM_CHAR_OBJECT_END:
			token.type = JAM_TOKEN_OBJECT_END;
			
			return token;
		case JAM_CHAR_COMMA:
			token.type = JAM_TOKEN_COMMA;
			
			return token;
		case JAM_CHAR_COLON:
			token.type = JAM_TOKEN_COLON;
			
			return token;
		case JAM_CHAR_COMMENT_START:
			while (jam_next_char(deserializer),
				deserializer->current_char != JAM_CHAR_EOF &&
				deserializer->current_char != JAM_CHAR_LF
			) {}
			
			deserializer->prev_char = deserializer->current_char;
			
			continue;
		case JAM_CHAR_QUOTE:
			return tokenize_string(deserializer);
		default:
			return tokenize_word(deserializer);
		}
	}
}

void jam_print_token(struct jam_token token) {
	switch (token.type) {
	case JAM_TOKEN_EOF:
		printf("End of file\n");
		
		break;
	case JAM_TOKEN_FLAVOR:
		printf("Flavor: %s\n", token.contents.as_string);
		
		break;
	case JAM_TOKEN_NULL:
		printf("Null\n");
		
		break;
	case JAM_TOKEN_BOOL:
		printf("Bool: %i\n", token.contents.as_bool);
		
		break;
	case JAM_TOKEN_NUMBER:
		printf("Number: %f\n", token.contents.as_number);
		
		break;
	case JAM_TOKEN_STRING:
		printf("String: %s\n", token.contents.as_string);
		
		break;
	case JAM_TOKEN_LIST_START:
		printf("List start\n");
		
		break;
	case JAM_TOKEN_LIST_END:
		printf("List end\n");
		
		break;
	case JAM_TOKEN_OBJECT_START:
		printf("Object start\n");
		
		break;
	case JAM_TOKEN_OBJECT_END:
		printf("Object end\n");
		
		break;
	case JAM_TOKEN_COMMA:
		printf("Comma\n");
		
		break;
	case JAM_TOKEN_COLON:
		printf("Colon\n");
		
		break;
	default:
		printf("Invalid token\n");
		
		break;
	}
}
